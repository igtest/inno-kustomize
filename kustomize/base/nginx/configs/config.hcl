// ##CH ME##
// listen {
//   port = 4040
// }

//     enable_experimental = true

// namespace "nginx" {
//   source = {
//     syslog {
//       listen_address = "udp://127.0.0.1:8154"
//       format = "rfc3164"
//       tags = ["nginx"]
//     }
//   }

//   format = "$remote_addr - [$time_local] \"$request\" $status $body_bytes_sent \"$http_referer\" \"$http_user_agent\" $http_x_forwarded_for $request_time $upstream_response_time $proxy_host $upstream_addr"
//   relabel "upstream_addr" { from = "upstream_addr" }
//   relabel "proxy_host" { from = "proxy_host" }

//   labels {
//     app = "nginx"
//   }
// }
